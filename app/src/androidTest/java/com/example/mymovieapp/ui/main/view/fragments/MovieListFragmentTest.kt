package com.example.mymovieapp.ui.main.view.fragments

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.findNavController
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.mymovieapp.R
import com.example.mymovieapp.ui.main.view.activities.MainActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MovieListFragmentTest {

    @get:Rule
    val scenarioRuleRule = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun setup() {
        scenarioRuleRule.scenario.onActivity {
            it.findNavController(R.id.nav_host_fragment).navigate(R.id.action_movieListFragment_to_movieDetailFragment, null)
        }
    }

    @Test
    fun test_isMovieDataVisible() {
//        val scenario = launchFragmentInContainer<MovieListFragment>()

        onView(withId(R.id.recyclerView)).check(matches(isDisplayed()))
    }
}