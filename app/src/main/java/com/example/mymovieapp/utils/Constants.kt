package com.example.mymovieapp.utils

class Constants {
    companion object {
        const val IMAGE_URL = "imageUrl"
        const val SUMMARY_SHORT = "summaryShort"
    }
}