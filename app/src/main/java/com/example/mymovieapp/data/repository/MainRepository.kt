package com.example.mymovieapp.data.repository

import com.example.mymovieapp.data.api.ApiService

class MainRepository constructor(private val apiService: ApiService) {
    suspend fun getMovieList() = apiService.getMovieList()
}