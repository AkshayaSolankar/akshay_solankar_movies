package com.example.mymovieapp.data.api

import com.example.mymovieapp.data.model.MovieModel
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {
    @GET("all.json?order=by-opening-date&api-key=861p4Y0GGKcX90BWWCJw9Jem56hy3w7Q")
    suspend fun getMovieList(): Response<MovieModel>
}