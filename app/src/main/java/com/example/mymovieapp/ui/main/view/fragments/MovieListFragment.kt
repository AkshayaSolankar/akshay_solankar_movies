package com.example.mymovieapp.ui.main.view.fragments

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.mymovieapp.R
import com.example.mymovieapp.data.api.RetrofitBuilder
import com.example.mymovieapp.ui.main.adapter.MainAdapter
import com.example.mymovieapp.data.model.Result
import com.example.mymovieapp.data.repository.MainRepository
import com.example.mymovieapp.ui.base.BaseFragment
import com.example.mymovieapp.ui.main.viewmodel.MainViewModel
import com.example.mymovieapp.ui.main.viewmodel.ViewModelFactory
import com.example.mymovieapp.utils.Constants
import kotlinx.android.synthetic.main.fragment_movie_list.*

class MovieListFragment : BaseFragment(), MainAdapter.OnItemClickListener {

    private var moviesList: List<Result>? = null
    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: MainAdapter

    override fun layoutId() = R.layout.fragment_movie_list

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewModel()
        setupUI()
        setupObservers()
    }

    private fun setupViewModel() {
        val apiService = RetrofitBuilder.apiService
        viewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(
                MainRepository(apiService)
            )
        ).get(MainViewModel::class.java)
        viewModel.getMovieList()
    }

    private fun setupUI() {
        adapter = MainAdapter(this, arrayListOf())
        recyclerView?.adapter = adapter
    }

    private fun setupObservers() {
        viewModel.getMovieListLiveData().observe(viewLifecycleOwner, Observer {
            retrieveList(it.results)
        })
        viewModel.errorLoadingData().observe(viewLifecycleOwner, {
            progressBar?.visibility = View.GONE
            Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
        })
    }

    private fun retrieveList(movies: List<Result>?) {
        recyclerView?.visibility = View.VISIBLE
        progressBar?.visibility = View.GONE
        adapter.apply {
            moviesList = movies
            addMovies(movies)
            notifyDataSetChanged()
        }
    }

    override fun onItemClick(position: Int) {
        val summaryShort = moviesList?.get(position)?.summaryShort
        val imageUrl = moviesList?.get(position)?.multimedia?.src
        val bundle = Bundle()
        bundle.putString(Constants.IMAGE_URL, imageUrl)
        bundle.putString(Constants.SUMMARY_SHORT, summaryShort)
        findNavController().navigate(R.id.action_movieListFragment_to_movieDetailFragment, bundle)
    }
}