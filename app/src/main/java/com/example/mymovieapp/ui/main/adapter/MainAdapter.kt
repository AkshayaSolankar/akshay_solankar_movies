package com.example.mymovieapp.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mymovieapp.R
import com.example.mymovieapp.data.model.Result
import com.example.mymovieapp.ui.main.view.fragments.MovieListFragment
import kotlinx.android.synthetic.main.row_item_layout.view.*

class MainAdapter(
    private val listener: MovieListFragment,
    private val movieList: ArrayList<Result>
) :
    RecyclerView.Adapter<MainAdapter.DataViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder =
        DataViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.row_item_layout, parent, false)
        )

    override fun getItemCount(): Int = movieList.size


    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        holder.bind(movieList[position])
    }

    fun addMovies(movies: List<Result>?) {
        this.movieList.apply {
            clear()
            movies?.let { addAll(it) }
        }
    }

    inner class DataViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        fun bind(movie: Result) {
            itemView.apply {
                tvMovieTitle.text = movie.displayTitle
                Glide.with(ivMovieThumbnail.context)
                    .load(movie.multimedia?.src)
                    .into(ivMovieThumbnail)
            }
        }

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = absoluteAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listener.onItemClick(position)
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }
}