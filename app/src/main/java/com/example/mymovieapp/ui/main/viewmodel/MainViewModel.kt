package com.example.mymovieapp.ui.main.viewmodel


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mymovieapp.data.model.MovieModel
import com.example.mymovieapp.data.repository.MainRepository
import kotlinx.coroutines.*

class MainViewModel(private val mainRepository: MainRepository) : ViewModel() {

    private var job: Job? = null
    private val movieList = MutableLiveData<MovieModel>()
    private val errorMessage = MutableLiveData<String>()

    fun getMovieList() {
        job = CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = mainRepository.getMovieList()
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        movieList.postValue(response.body())
                    } else {
                        onError("Error : ${response.message()}")
                    }
                }
            } catch (e: Exception) {
                e.message?.let { onError(it) }
            }
        }
    }

    fun getMovieListLiveData(): LiveData<MovieModel> {
        return movieList
    }

    fun errorLoadingData(): LiveData<String> {
        return errorMessage
    }

    private fun onError(message: String) {
        errorMessage.postValue(message)
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }
}