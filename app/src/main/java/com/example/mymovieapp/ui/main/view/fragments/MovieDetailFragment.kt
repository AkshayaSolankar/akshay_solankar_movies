package com.example.mymovieapp.ui.main.view.fragments

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.bumptech.glide.Glide
import com.example.mymovieapp.R
import com.example.mymovieapp.ui.base.BaseFragment
import com.example.mymovieapp.utils.Constants
import kotlinx.android.synthetic.main.fragment_movie_detail.*

class MovieDetailFragment : BaseFragment() {

    private var summaryShort: String? = null
    private var imageUrl: String? = null

    override fun layoutId() = R.layout.fragment_movie_detail

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        imageUrl = arguments?.getString(Constants.IMAGE_URL)
        summaryShort = arguments?.getString(Constants.SUMMARY_SHORT)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (imageUrl != null && !TextUtils.isEmpty(summaryShort)) {
            tvMovieSummary.text = summaryShort
            Glide.with(ivMovieSummary.context)
                .load(imageUrl)
                .into(ivMovieSummary)
        }
    }

}